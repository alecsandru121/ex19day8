import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.Arrays;
import java.util.Scanner;

public class CreateFile {

    private File newFile;
    private BufferedWriter writeToFile;
    private String path;
    private Scanner input;
    private String[] Array;

    public void openFile(){

        input = new Scanner(System.in);

        path = input.next();

        try {

            newFile = new File(path);

            newFile.createNewFile();
            System.out.println("The file was created.");

        } catch(FileAlreadyExistsException e) {
            System.out.println("File already exists.");
        } catch (IOException e) {
            System.out.println("Another I/O error");}
    }
    public void myRecords() throws IOException{

        System.out.println("Enter the number of elements you want to write to the file.");

        input = new Scanner(System.in);

        int arrayLength = input.nextInt();

        Array= new String[arrayLength];

        for (int i = 0; i < Array.length ; i++) {
            System.out.println("Enter a new element.");
            String element = input.next();
            Array[i] = element;

            System.out.println("Entered element is [" + Array[i] + "]");
        }
        System.out.println("The elements that will be written to the file are: " + Arrays.toString(Array));

        writeToFile = new BufferedWriter(new FileWriter(path));

        for (String element:Array) {
            writeToFile.write(element);
            writeToFile.newLine();
        }
    }
    public void closeFile(){
        try {

            writeToFile.close();

            System.out.println("Check the file in " + path);
        } catch (IOException e) {
            System.out.println("Error!");
        }
    }
}